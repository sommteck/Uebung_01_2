using System;
using System.IO;
// using System.Collections.Generic;
// using System.Linq;
// using System.Text;
// using System.Threading.Tasks;

namespace Uebung_01_2
{
    class Program
    {

        static void Main(string[] args)
        {
            int i = 123;

            string s;

            Console.Write(i);

            Console.WriteLine(1.2345);

            s = Zusatz.Test_1();
            Console.WriteLine(s);

            s = Zusatz.Test_2("Welt");
            Console.WriteLine(s);

            s = Zusatz.Test_2("Hallo", "Welt");
            Console.WriteLine(s);

            Console.ReadKey();
        }

    }

    class Zusatz
    {
        public void Test()
        {
        
        }

        public static string Test_1()
        {
            return "Hallo!";
        }

        public static string Test_2(string a)
        {
            return "Hallo " + a + "!";
        }

        public static string Test_2(string a, string b)
        {
            return a + " " + b + "!";
        }
        
    }
}
